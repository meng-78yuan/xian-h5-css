//获取dom
let datas = {
	headerImg:"https://ts1.cn.mm.bing.net/th/id/R-C.b731285888e43842c784ad86402e4dbf?rik=MikiLtOXCX3gmg&riu=http%3a%2f%2fpic30.photophoto.cn%2f20140121%2f0020033057664207_b.jpg&ehk=7hvxbhlGrzjYBMJDkUbHpdCmtLD%2fke%2bb6A%2bsMNhg2rM%3d&risl=&pid=ImgRaw&r=0",
	router:["网站首页","文化西安","秦腔"],
	content:[
		{
			title:"总体概况",
			detail:[`秦腔（梆子腔，乱弹），中国西北地区传统戏剧，起于西周，源于西府，成熟于秦，是四大声腔中最古老、最丰富的声腔体系。`],
			img:"https://pic.baike.soso.com/ugc/baikepic2/0/ori-20191126095047-1100146605_jpg_551_363_25269.jpg/800"
		},
		{
			title:"剧种起源",
			detail:[`秦腔，是起源于古代陕西的民间歌舞，是在中国古代政治、经济、文化中心长安生长壮大起来的，经历代人民的创造而逐渐形成，因周代以来，关中地区就被称为"秦"，秦腔由此而得名，是相当古老的剧种之一。因以枣木梆子为击节乐器，又叫"梆子腔"，因以梆击节时发出"恍恍"声，俗称"桄桄子"。清人李调元《雨村剧话》云："俗传钱氏缀百裘外集，有秦腔。始于陕西，以梆为板，月琴应之，亦有紧慢，俗呼梆子腔，蜀谓之乱弹。""乱弹"一词在我国戏曲声腔中的含义很多，过去曾把昆曲、高腔之外的剧种都叫"乱弹"，也曾把京剧称为"乱弹"，还有的剧种也以乱弹命名，如温州乱弹、河北乱弹但是，更多的仍用在以秦腔为先、为主的梆子腔系统的总称上。`],
			img:"https://pic.baike.soso.com/ugc/baikepic2/0/20220324005130-902139872_jpeg_1772_1181_221210.jpg/800"
		},
		{
			title:"传统剧目",
			detail:[`秦腔所演的剧目有神话、民间故事和各种公案戏。它的传统剧目丰富，已抄存的共2748本。备受观众喜爱的曲目有《春秋笔》、《八义图》、《紫霞宫》、《和氏璧》、《惠凤扇》、《玉虎坠》、《麟骨床》、《鸳鸯被》、《射九阳》、《哭长城》、《伐董卓》、《白蛇传》、《梵王宫》、《法门寺》、《铁公鸡》、《长坂坡》、《卖华山》、《临潼山》、《斩单通》、《取洛阳》、《三娘教子》、《柜中缘》、《反延安》、《破洪州》、《三上殿》、《献西川》等。脍炙人口的曲目有《三滴血》、《周仁回府》、《十五贯》、《火焰驹》、《大登殿》等。新中国建立后还创作了《黄花岗》、《汉宫案》、《屈原》等脍炙人口的佳作。`],
			img:""
		}
	]
}

let img_main = document.getElementById("img_main");
img_main.src = datas.headerImg;

let router = document.getElementById("router");
for(let i=0,len=datas.router.length;i<len;i++){
	let t = document.createElement("div");
	t.innerHTML = datas.router[i] + (i+1!=len?" - ":"");
	t.setAttribute('class','router_text')
	router.appendChild(t);
}
//修改内容并创建目录
let catalog = document.getElementById("catalog");
let content = document.getElementById("content");
for(let i=0,value=datas.content,len=value.length;i<len;i++){
	let chapter = document.createElement("div");
	chapter.setAttribute('class','chapter')
	chapter.setAttribute('id','chapter_'+i)
	
	let title  = document.createElement("div");
	title.setAttribute('class','title')
	title.innerHTML = '<span class="daq-icon icon"></span>' + value[i].title;
	chapter.appendChild(title);
	
	for(let j=0,jlen=value[i].detail.length;j<jlen;j++){
		let detail = document.createElement("div");
		detail.setAttribute('class','detail');
		detail.innerHTML = value[i].detail[j];
		chapter.appendChild(detail);
	}
	
	let image_container =  document.createElement("div");
	image_container.setAttribute('class','content_img');
	let image  = document.createElement("img");
	image.setAttribute('class','image');
	image.src = value[i].img;
	image_container.appendChild(image);
	chapter.appendChild(image_container);
	
	content.appendChild(chapter);
	
	let catalog_item = document.createElement('div');
	catalog_item.setAttribute('class','catalog_item');
	if(i==0) catalog_item.setAttribute('class','catalog_item curr');
	catalog_item.setAttribute('id','catalog_item_' + i);
	catalog_item.innerHTML = value[i].title;
	catalog.appendChild(catalog_item);
}

window.addEventListener('scroll',()=>{
	let windowHeight = document.documentElement.clientHeight
	let threshold = 20 //指的是提前加载的距离
	
	//获取所有
	let targetElementArray = [];
	for(let i=0,len=datas.content.length;i<len;i++){
		let t = document.getElementById('chapter_'+i);
		targetElementArray.push([i,t.offsetTop]);
	}
	targetElementArray.sort((a,b)=>{
		return b[1]-a[1];
	});
	
	let scrollBarTop = window.scrollY;
	let cur = document.getElementsByClassName("curr")[0];
	console.log(targetElementArray)
	targetElementArray.some((value)=>{
		console.log(value[1],windowHeight, scrollBarTop + threshold)
		if(value[1]<( scrollBarTop + threshold)){
			cur?cur.setAttribute('class','catalog_item'):null;
			document.getElementById('catalog_item_'+ value[0]).setAttribute('class','catalog_item curr')
			return true;
		}
		
	})
})






