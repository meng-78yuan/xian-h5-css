//获取dom
let datas = {
	headerImg:"../img/article_dayanta.jpg",
	router:["网站首页","文化西安","大雁塔"],
	content:[
		{
			title:"总体概况",
			detail:[`位于大慈恩寺内，是玄奘法师为供奉从印度带回的佛像、舍利和梵文经典而建造的一座砖塔。大雁塔是楼阁式砖塔，塔身呈方形锥体，具有中国传统建筑艺术的风格。塔高64米，共七层，塔身枋、斗拱、栏额均为青砖仿木结构。`],
			img:""
		},
		{
			title:"建筑结构",
			detail:[`大雁塔是砖仿木结构的四方形楼阁式塔，由塔基、塔身、塔刹三部分组成。全塔通高64.7米，塔基高4.2米，南北长约48.7米，东西长约45.7米；塔身底层边长25.5米，呈方锥形；塔刹高4.87米。1、2两层有9间，3、4两层有7间，5、6、7、8层有五间，每层四面均有劵门。`],
			img:""
		},
		{
			title:"文物遗存",
			detail:[`大雁塔基座皆有石门，门楣门框上均有精美的线刻佛像及砖雕对联。底层南门洞两侧嵌置碑石，西龛是由右向左书写，唐太宗李世民亲自撰文、时任中书令的大书法家褚遂良手书的《大唐三藏圣教序》碑，东龛是由左向右书写，唐高宗李治撰文、褚遂良手书的《大唐三藏圣教序记》碑，人称“二圣三绝碑”。两碑规格形式相同，碑头为蟠螭圆首，碑身两边线有明显收分，呈上窄下宽的梯形（此为唐碑典型形制），碑座为有线刻图案的方形碑座，两碑通高337.5厘米，碑面上宽86厘米，下宽100厘米。碑文高度赞扬玄奘法师西天取经，弘扬佛法的历史功绩和非凡精神，世称《雁塔圣教》。`,`这两块碑石是唐高宗永徽四年（653年）十月由玄奘亲手竖立于此的，于今保存完好。是研究唐代书法、绘画、雕刻艺术的重要文物。值得一提的是，唐代画家吴道子，大诗人王维等曾为慈恩寺作过不少壁画，可惜早已湮没在历史中。但大雁塔下四门洞的石门楣、门框上，却还保留着精美的唐代线刻画。除此之外，南门的券洞两侧还嵌有“玄奘负笈图”和“玄奘译经图”`],
			img:"https://pic.baike.soso.com/ugc/baikepic2/4904/20220319085955-1418331597_jpeg_375_500_47363.jpg/0"
		}
	]
}

let img_main = document.getElementById("img_main");
img_main.src = datas.headerImg;

let router = document.getElementById("router");
for(let i=0,len=datas.router.length;i<len;i++){
	let t = document.createElement("div");
	t.innerHTML = datas.router[i] + (i+1!=len?" - ":"");
	t.setAttribute('class','router_text')
	router.appendChild(t);
}
//修改内容并创建目录
let catalog = document.getElementById("catalog");
let content = document.getElementById("content");
for(let i=0,value=datas.content,len=value.length;i<len;i++){
	let chapter = document.createElement("div");
	chapter.setAttribute('class','chapter')
	chapter.setAttribute('id','chapter_'+i)
	
	let title  = document.createElement("div");
	title.setAttribute('class','title')
	title.innerHTML = '<span class="daq-icon icon"></span>' + value[i].title;
	chapter.appendChild(title);
	
	for(let j=0,jlen=value[i].detail.length;j<jlen;j++){
		let detail = document.createElement("div");
		detail.setAttribute('class','detail');
		detail.innerHTML = value[i].detail[j];
		chapter.appendChild(detail);
	}
	
	let image_container =  document.createElement("div");
	image_container.setAttribute('class','content_img');
	let image  = document.createElement("img");
	image.setAttribute('class','image');
	image.src = value[i].img;
	image_container.appendChild(image);
	chapter.appendChild(image_container);
	
	content.appendChild(chapter);
	
	let catalog_item = document.createElement('div');
	catalog_item.setAttribute('class','catalog_item');
	if(i==0) catalog_item.setAttribute('class','catalog_item curr');
	catalog_item.setAttribute('id','catalog_item_' + i);
	catalog_item.innerHTML = value[i].title;
	catalog.appendChild(catalog_item);
}

window.addEventListener('scroll',()=>{
	let windowHeight = document.documentElement.clientHeight
	let threshold = 20 //指的是提前加载的距离
	
	//获取所有
	let targetElementArray = [];
	for(let i=0,len=datas.content.length;i<len;i++){
		let t = document.getElementById('chapter_'+i);
		targetElementArray.push([i,t.offsetTop]);
	}
	targetElementArray.sort((a,b)=>{
		return b[1]-a[1];
	});
	
	let scrollBarTop = window.scrollY;
	let cur = document.getElementsByClassName("curr")[0];
	console.log(targetElementArray)
	targetElementArray.some((value)=>{
		console.log(value[1],windowHeight, scrollBarTop + threshold)
		if(value[1]<( scrollBarTop + threshold)){
			cur?cur.setAttribute('class','catalog_item'):null;
			document.getElementById('catalog_item_'+ value[0]).setAttribute('class','catalog_item curr')
			return true;
		}
		
	})
})






