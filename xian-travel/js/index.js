var swiperImgSrc = [
	"img/swiperalt3.jpg",
	"img/swiperalt2.jpg",
	"img/swiperalt4.jpg"
]
var Feature = [
	{
		tourname:"大雁塔",
		imgSrc:"img/dayanta.png",
		textInfo:"作为现存最早、规模最大的唐代四方楼阁式砖塔,凝聚了中国古代劳动人民智慧结晶的标志性建筑。",
		href:"component/article_dayanta.html"
		},
		
	{
		tourname:"秦始皇兵马俑博物馆",
		imgSrc:"img/bingmayong.jpg",
		textInfo:"秦始皇兵马俑博物馆布局缜密、规模宏富，具有重大的历史、科学和艺术价值。",
		href:"component/article_bingmayong.html"
	},
]

//var topbar =  document.getElementsByClassName("timestamp");
var p_feature_first_name = document.getElementById("p_feature_first_name");
var p_feature_first_info = document.getElementById("p_feature_first_info");
var a_feature_first = document.getElementById("a_feature_first");

var p_feature_second_name = document.getElementById("p_feature_second_name");
var p_feature_second_info = document.getElementById("p_feature_second_info");
var a_feature_second = document.getElementById("a_feature_second");

var img_feature_first = document.getElementById("img_feature_first");
var img_feature_second = document.getElementById("img_feature_second");
var swiper_wrapper = document.getElementById("swiper-wrapper")

//swiper_alt.src = swiperImgSrc[1]
console.log(img_feature_first);
p_feature_first_name.innerHTML =  Feature[0].tourname;
p_feature_first_info.innerHTML =  Feature[0].textInfo;
a_feature_first.href =  Feature[0].href;

p_feature_second_name.innerHTML =  Feature[1].tourname;
p_feature_second_info.innerHTML =  Feature[1].textInfo;
a_feature_second.href = Feature[1].href;

img_feature_first.src = Feature[0].imgSrc;
img_feature_second.src = Feature[1].imgSrc;

for(let i=0,len=swiperImgSrc.length;i<len;i++){
	
	let slide = document.createElement("div");
	
	let slide_img = document.createElement("img");
	slide_img.src =  swiperImgSrc[i];
	slide_img.setAttribute('class','img_swiper')
	slide.appendChild(slide_img)
	
	slide.setAttribute('class','swiper-slide')
	swiper_wrapper.appendChild(slide)
	console.log(slide)
}
