//获取dom
let datas = {
	headerImg:"https://youimg1.c-ctrip.com/target/0104n1200086wx100431F_D_10000_1200.jpg?proc=autoorient",
	router:["网站首页","文化西安","碑林"],
	content:[
		{
			title:"总体概况",
			detail:[`西安碑林位于著名古城西安市三学街。陕西西安碑林始建于宋哲宗元祐二年（1087），经金、元、明、清、民国历代的维修及增建，规模不断扩大，藏石日益增多，现收藏自汉代至今的碑石、墓志4000余件，数量为全国之最，藏品时代系列完整，时间跨度达2000多年。`],
			img:"https://pic.baike.soso.com/ugc/baikepic2/2044/20200605012851-2121905322_jpeg_830_663_86945.jpg/0"
		},
		{
			title:"建筑布局",
			detail:[`西安碑林博物馆占地面积3.19万平方米，陈列面积4900平方米，由孔庙、碑林、石刻艺术室和石刻艺术馆分组成。西安孔庙唐时在尚书省西隅国子监附近。宋代几经搬迁，北宋崇宁二年（1103年）虞策将文庙、府学最终迁建于“府城之东南隅”，即西安碑林博物馆现址。现存的照壁、牌坊、棂星门、华表、戟门、碑亭、两庑多为明清建筑。西安碑林始建于宋元祐二年（1087年），经金、元、明、清、民国历代的维修及增建，规模不断扩大，藏石日益增多，现收藏自汉代之后的碑石、墓志4000余件，数量为全国之最，藏品时代系列完整，时间跨度达2000多年。其中，陈列在碑亭中的《石台孝经》，刻于唐天宝四年（745年），由唐玄宗李隆基注书，被誉为“迎宾第一碑”，是最早移入碑林的藏品之一。`],
			img:"http://5b0988e595225.cdn.sohucs.com/images/20191206/dda2532833f842189716921d6bebc207.jpeg"
		},
		{
			title:"馆藏文物",
			detail:[`西安碑林博物馆藏品以历代碑石、墓志及石刻造像为主，截至2019年末，西安碑林博物馆藏品有13568件/套，其中珍贵文物2281件/套。重要藏品包括汉《曹全碑》、颜真卿《多宝塔碑》、唐太宗昭陵六骏石刻等。`],
			img:"http://5b0988e595225.cdn.sohucs.com/images/20190325/c2014cbd2f67485aab74e29af0999195.jpeg"
		},{
			title:"历史价值",
			detail:[`西安碑林坐落于著名古城西安市三学街（因清代的长安学、府学、咸宁学均设在这里而得此名）。西安碑林是我国收藏古代碑石墓志时间最早、名碑最多的一座艺术宝库，它不仅是中国古代文化典籍刻石的集中地点之一，也是历代著名书法艺术珍品的荟萃之地，有着巨大的历史和艺术价值。`],
			img:"https://youimg1.c-ctrip.com/target/fd/tg/g5/M0A/A3/D4/CggYsVbESNWAKBVtAAK5R66Lvsw636_R_640_10000_Q90.jpg"
		}
		
	]
}

let img_main = document.getElementById("img_main");
img_main.src = datas.headerImg;

let router = document.getElementById("router");
for(let i=0,len=datas.router.length;i<len;i++){
	let t = document.createElement("div");
	t.innerHTML = datas.router[i] + (i+1!=len?" - ":"");
	t.setAttribute('class','router_text')
	router.appendChild(t);
}
//修改内容并创建目录
let catalog = document.getElementById("catalog");
let content = document.getElementById("content");
for(let i=0,value=datas.content,len=value.length;i<len;i++){
	let chapter = document.createElement("div");
	chapter.setAttribute('class','chapter')
	chapter.setAttribute('id','chapter_'+i)
	
	let title  = document.createElement("div");
	title.setAttribute('class','title')
	title.innerHTML = '<span class="daq-icon icon"></span>' + value[i].title;
	chapter.appendChild(title);
	
	for(let j=0,jlen=value[i].detail.length;j<jlen;j++){
		let detail = document.createElement("div");
		detail.setAttribute('class','detail');
		detail.innerHTML = value[i].detail[j];
		chapter.appendChild(detail);
	}
	
	let image_container =  document.createElement("div");
	image_container.setAttribute('class','content_img');
	let image  = document.createElement("img");
	image.setAttribute('class','image');
	image.src = value[i].img;
	image_container.appendChild(image);
	chapter.appendChild(image_container);
	
	content.appendChild(chapter);
	
	let catalog_item = document.createElement('div');
	catalog_item.setAttribute('class','catalog_item');
	if(i==0) catalog_item.setAttribute('class','catalog_item curr');
	catalog_item.setAttribute('id','catalog_item_' + i);
	catalog_item.innerHTML = value[i].title;
	catalog.appendChild(catalog_item);
}

window.addEventListener('scroll',()=>{
	let windowHeight = document.documentElement.clientHeight
	let threshold = 20 //指的是提前加载的距离
	
	//获取所有
	let targetElementArray = [];
	for(let i=0,len=datas.content.length;i<len;i++){
		let t = document.getElementById('chapter_'+i);
		targetElementArray.push([i,t.offsetTop]);
	}
	targetElementArray.sort((a,b)=>{
		return b[1]-a[1];
	});
	
	let scrollBarTop = window.scrollY;
	let cur = document.getElementsByClassName("curr")[0];
	console.log(targetElementArray)
	targetElementArray.some((value)=>{
		console.log(value[1],windowHeight, scrollBarTop + threshold)
		if(value[1]<( scrollBarTop + threshold)){
			cur?cur.setAttribute('class','catalog_item'):null;
			document.getElementById('catalog_item_'+ value[0]).setAttribute('class','catalog_item curr')
			return true;
		}
		
	})
})






