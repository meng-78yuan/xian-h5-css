//获取dom
let datas = {
	headerImg:"https://static.pptx.cn/wp-content/uploads/2022/03/1646501942-manoj-kumar-kasirajan-fAZEw6xPhz8-unsplash.jpg",
	router:["网站首页","文化西安","兵马俑"],
	content:[
		{
			title:"总体概况",
			detail:[`兵马俑，即秦始皇兵马俑，亦简称秦兵马俑或秦俑，是古代墓葬雕塑的一个类别，位于今陕西省西安市临潼区秦始皇陵以东1.5千米处的兵马俑坑内，是第一批全国重点文物保护单位、第一批中国世界遗产，被誉为“世界第八大奇迹”。`],
			img:"https://pic.baike.soso.com/ugc/baikepic2/0/20220402162547-1558963566_png_692_407_195324.jpg/800"
		},
		{
			title:"文物特征",
			detail:[`秦兵马俑的特点：规模宏大，类型众多，个性鲜明，神态各异。秦始皇陵兵马俑是世界最大的地下军事博物馆。俑坑布局合理，结构奇特，在深5米左右的坑底，每隔3米架起一道东向西的承重墙，兵马俑排列在墙间空档的过洞中。秦陵内共有3个兵马俑坑，呈品字形排列。秦始皇陵一号俑坑，呈长方形，东西长230米，南北宽62米，深约5米，总面积14260平方米，四面有斜坡门道。俑坑中最多的是武士俑，平均身高1.80米左右，最高的1.90米以上，陶马高1.72米，长2.03米，战车与实用车的大小 一样。但兵马俑并非按原比例还原，据记载秦国人的身高在165cm左右。秦俑大部分手执青铜兵器，有弓、弩、箭镞、铍、矛、戈、殳、剑、弯刀和钺。青铜兵器因经过防锈处理，埋在地下两千多年，至今仍然光亮锋利如新，它们是当时的实战武器，身穿甲片细密的铠甲，胸前有彩线挽成的结穗。军吏头戴长冠，数量比武将多。秦俑的脸型、身材、表情、眉毛、眼睛和年龄都有不同之处。`,`俑坑发现种类齐全，数量空前的青铜兵器极大地丰富了秦兵器研究的领域，其中长铍，金钩等都是兵器考古史上的首次发现。兵器铸造的标准化工艺、兵器表面防腐处理技术的发现和研究填补了古代科技史研究的空白。而秦俑的设计者为了再现2000年前的秦军“奋击百万”气吞山河的磅礴气势，他们不仅仅在于追求单个陶俑的形体高大，而且是精心设计了一个由8000余件形体高大的俑群构成一组规模庞大的军阵体系。右侧为一个巨大的方阵，左前方为一个大型疏阵，左后方则是指挥部。那数千名手执兵器的武士，数百匹曳车的战马，一列列、一行行，构成规模宏伟、气势磅礴的阵容。有的头挽发髻，身穿战袍，足登短靴，手持弓弩，似为冲锋陷阵的锐士；有的免盔束发，外披铠甲，手持弓弩，背负铜镞，似为机智善射的弓箭手；有的头戴软帽，穿袍着甲，足登方口浅履，手持长铍，似为短兵相接的甲士。`],
			img:"https://pic.baike.soso.com/ugc/baikepic2/12497/20220318211531-1691571343_jpeg_800_521_81993.jpg/0"
		},
		{
			title:"制作技术",
			detail:[`秦兵马俑多用陶冶烧制的方法制成，先用陶模做出初胎，再覆盖一层细泥进行加工刻划加彩，有的先烧后接，有的先接再烧。其实秦始皇时期的兵马俑各个都有鲜艳和谐的彩绘。当年工匠犯了一个错，他们在烧制过后才上色，在发掘过程中发现有的陶俑刚出土时局部还保留着鲜艳的颜色，但是出土后由于被氧气氧化，颜色不到一个小时瞬间消尽，化作白灰。能看到的只是残留的彩绘痕迹。兵马俑的车兵、步兵、骑兵列成各种阵势。整体风格浑厚、健壮、洗练。如果仔细观察，脸型、发型、体态、神韵均有差异：陶马有的双耳竖立，有的张嘴嘶鸣，有的闭嘴静立。所有这些兵马俑都富有感染人的艺术魅力。`],
			img:"https://pic.baike.soso.com/ugc/baikepic2/1706/20220318211514-362216812_jpeg_1000_667_151184.jpg/0"
		},
		{
			title:"历史意义",
			detail:[`秦始皇陵兵马俑，不但对研究军事史上有着巨大的价值，而且对艺术史、科学史的研究具有独特的价值，它再现了2200年前中国雕塑艺术的辉煌成就，为世界了解中国古代文明提供了有利的条件。`,`秦始皇陵兵马俑被称为“20世纪最重要的考古发现”它和埃及、希腊等地的艺术精品的不同之处是以磅礴的气势、巨大的规模、严整独特的艺术结构震惊世界。它使我们仿佛置身于秦朝，看到了雄才大略的秦始皇正指挥着千军万马，统一六国的伟大事业。`],
			img:"https://pic.baike.soso.com/ugc/baikepic2/0/20220319044347-943005211_png_992_621_1338848.jpg/800"
		},
		
	]
}

let img_main = document.getElementById("img_main");
img_main.src = datas.headerImg;

let router = document.getElementById("router");
for(let i=0,len=datas.router.length;i<len;i++){
	let t = document.createElement("div");
	t.innerHTML = datas.router[i] + (i+1!=len?" - ":"");
	t.setAttribute('class','router_text')
	router.appendChild(t);
}
//修改内容并创建目录
let catalog = document.getElementById("catalog");
let content = document.getElementById("content");
for(let i=0,value=datas.content,len=value.length;i<len;i++){
	let chapter = document.createElement("div");
	chapter.setAttribute('class','chapter')
	chapter.setAttribute('id','chapter_'+i)
	
	let title  = document.createElement("div");
	title.setAttribute('class','title')
	title.innerHTML = '<span class="daq-icon icon"></span>' + value[i].title;
	chapter.appendChild(title);
	
	for(let j=0,jlen=value[i].detail.length;j<jlen;j++){
		let detail = document.createElement("div");
		detail.setAttribute('class','detail');
		detail.innerHTML = value[i].detail[j];
		chapter.appendChild(detail);
	}
	
	let image_container =  document.createElement("div");
	image_container.setAttribute('class','content_img');
	let image  = document.createElement("img");
	image.setAttribute('class','image');
	image.src = value[i].img;
	image_container.appendChild(image);
	chapter.appendChild(image_container);
	
	content.appendChild(chapter);
	
	let catalog_item = document.createElement('div');
	catalog_item.setAttribute('class','catalog_item');
	if(i==0) catalog_item.setAttribute('class','catalog_item curr');
	catalog_item.setAttribute('id','catalog_item_' + i);
	catalog_item.innerHTML = value[i].title;
	catalog.appendChild(catalog_item);
}

window.addEventListener('scroll',()=>{
	let windowHeight = document.documentElement.clientHeight
	let threshold = 20 //指的是提前加载的距离
	
	//获取所有
	let targetElementArray = [];
	for(let i=0,len=datas.content.length;i<len;i++){
		let t = document.getElementById('chapter_'+i);
		targetElementArray.push([i,t.offsetTop]);
	}
	targetElementArray.sort((a,b)=>{
		return b[1]-a[1];
	});
	
	let scrollBarTop = window.scrollY;
	let cur = document.getElementsByClassName("curr")[0];
	console.log(targetElementArray)
	targetElementArray.some((value)=>{
		console.log(value[1],windowHeight, scrollBarTop + threshold)
		if(value[1]<( scrollBarTop + threshold)){
			cur?cur.setAttribute('class','catalog_item'):null;
			document.getElementById('catalog_item_'+ value[0]).setAttribute('class','catalog_item curr')
			return true;
		}
		
	})
})






