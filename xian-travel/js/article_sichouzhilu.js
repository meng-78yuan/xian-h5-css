//获取dom
let datas = {
	headerImg:"https://img.zcool.cn/community/01b8d45de0cfc9a8012159720bd1db.jpg@1280w_1l_2o_100sh.jpg",
	router:["网站首页","文化西安","丝绸之路起点"],
	content:[
		{
			title:"总体概况",
			detail:[`汉唐长安因丝绸之路而伟大，丝绸之路因汉唐长安而不朽。建设“丝绸之路经济带”的宏伟目标，为西安这座千年古都实现复兴开启了新的发展机遇。西安是古代丝绸之路的起点，在全国区域经济布局上，西安作为连接丝绸之路国家的陇海兰新铁路沿线最大的西部中心城市，具有承东启西、连接南北的重要战略地位。`],
			img:"https://tse2-mm.cn.bing.net/th/id/OIP-C.lzxO83nDhrayszkmGqSaJwHaE8?pid=ImgDet&rs=1"
		},
		{
			title:"历史沿袭",
			detail:[`丝绸之路，是指中国古代经中亚通往南亚、西亚以及欧洲、北非的陆上贸易通道，因大量中国丝绸经此道西运，故称“丝绸之路”。这条大动脉贯通了当时人类文明发展的中心——亚、欧、非3个大陆，导致黄河流域的中华古文明、印度河流域的印度古文明、两河流域的希腊古文明、尼罗河流域的埃及古文明以及欧洲大陆的罗马古文明交流融合，也促进了佛教、祆(读音:xian，一声)教、基督教、摩尼教和伊斯兰教向东西传播，给人类文明发展史以极大的影响。`],
			img:"https://img1.qunarzz.com/travel/d5/1711/fd/d78de4b7e54bcfb5.jpg_r_1360x1360x95_ffa8f309.jpg"
		},
		{
			title:"城市复兴",
			detail:[`随着“丝绸之路经济带”建设的推进，陕西加快建设丝绸之路经济带航空城和铁路物流集散中心，组建中亚五国能源交易平台；增开国际航线和货运班机，提升“长安号”营运能力，促进陆空运输一体化和交通、物流、信息设施互联互通。西安将承担更加艰巨的任务，也将迎来新的发展机遇`],
			img:"../img/xian.jpg"
		}
	]
}

let img_main = document.getElementById("img_main");
img_main.src = datas.headerImg;

let router = document.getElementById("router");
for(let i=0,len=datas.router.length;i<len;i++){
	let t = document.createElement("div");
	t.innerHTML = datas.router[i] + (i+1!=len?" - ":"");
	t.setAttribute('class','router_text')
	router.appendChild(t);
}
//修改内容并创建目录
let catalog = document.getElementById("catalog");
let content = document.getElementById("content");
for(let i=0,value=datas.content,len=value.length;i<len;i++){
	let chapter = document.createElement("div");
	chapter.setAttribute('class','chapter')
	chapter.setAttribute('id','chapter_'+i)
	
	let title  = document.createElement("div");
	title.setAttribute('class','title')
	title.innerHTML = '<span class="daq-icon icon"></span>' + value[i].title;
	chapter.appendChild(title);
	
	for(let j=0,jlen=value[i].detail.length;j<jlen;j++){
		let detail = document.createElement("div");
		detail.setAttribute('class','detail');
		detail.innerHTML = value[i].detail[j];
		chapter.appendChild(detail);
	}
	
	let image_container =  document.createElement("div");
	image_container.setAttribute('class','content_img');
	let image  = document.createElement("img");
	image.setAttribute('class','image');
	image.src = value[i].img;
	image_container.appendChild(image);
	chapter.appendChild(image_container);
	
	content.appendChild(chapter);
	
	let catalog_item = document.createElement('div');
	catalog_item.setAttribute('class','catalog_item');
	if(i==0) catalog_item.setAttribute('class','catalog_item curr');
	catalog_item.setAttribute('id','catalog_item_' + i);
	catalog_item.innerHTML = value[i].title;
	catalog.appendChild(catalog_item);
}

window.addEventListener('scroll',()=>{
	let windowHeight = document.documentElement.clientHeight
	let threshold = 20 //指的是提前加载的距离
	
	//获取所有
	let targetElementArray = [];
	for(let i=0,len=datas.content.length;i<len;i++){
		let t = document.getElementById('chapter_'+i);
		targetElementArray.push([i,t.offsetTop]);
	}
	targetElementArray.sort((a,b)=>{
		return b[1]-a[1];
	});
	
	let scrollBarTop = window.scrollY;
	let cur = document.getElementsByClassName("curr")[0];
	console.log(targetElementArray)
	targetElementArray.some((value)=>{
		console.log(value[1],windowHeight, scrollBarTop + threshold)
		if(value[1]<( scrollBarTop + threshold)){
			cur?cur.setAttribute('class','catalog_item'):null;
			document.getElementById('catalog_item_'+ value[0]).setAttribute('class','catalog_item curr')
			return true;
		}
		
	})
})






